from support import *
from statistics import mode


# --- Day 1: Sonar Sweep ---
def day_one_one(data_list=None, day='Day one.one'):
    num_depth_increases = 0
    if data_list is None:
        with open('input.txt') as file:
            data_list = list(file)
    for index in range(len(data_list)-1):
        if int(data_list[index]) < int(data_list[index+1]):
            num_depth_increases += 1

    print(f'{day} answer = {num_depth_increases}')


# ---Part Two---
def day_one_two():
    three_measurement_sliding_window_sums = []
    with open('input.txt') as file:
        data_list = list(file)
    while len(data_list) >= 3:
        num_sum = 0
        for index in range(3):
            num_sum += int(data_list[index])
        three_measurement_sliding_window_sums.append(num_sum)
        data_list.pop(0)
    day_one_one(data_list=three_measurement_sliding_window_sums, day='Day one.two')


# --- Day 2: Dive! ---
def day_two_one(day='Day two.one'):
    horizontal_pos = 0
    vertical_pos = 0
    with open('input_2.txt') as file:
        data_list = list(file)
        for entry in data_list:
            key, value = entry.split()
            if key == 'forward':
                horizontal_pos += int(value)
            elif key == 'down':
                vertical_pos += int(value)
            elif key == 'up':
                vertical_pos -= int(value)
    print(f'{day} answer = {horizontal_pos * vertical_pos}')


# --- Part Two ---
def day_two_two(day='Day two.two'):
    horizontal_pos = 0
    vertical_pos = 0
    aim = 0
    with open('input_2.txt') as file:
        data_list = list(file)
        for entry in data_list:
            key, value = entry.split()
            if key == 'forward':
                if not aim:
                    horizontal_pos += int(value)
                else:
                    horizontal_pos += int(value)
                    vertical_pos += int(value)*aim
            elif key == 'down':
                aim += int(value)
            elif key == 'up':
                aim -= int(value)
    print(f'{day} answer = {horizontal_pos * vertical_pos}')


# --- Day 3: Binary Diagnostic ---
def day_three_one(day='Day three.one'):
    bit_data_frame = create_data_frame('input_3.txt')
    gamma_rate = ''
    epsilom_rate = ''
    # for each column in the dataframe, find most common bit
    for column in bit_data_frame.columns:
        most_common_bit = mode(bit_data_frame[column].tolist())
        gamma_rate += most_common_bit
        if most_common_bit == '1':
            epsilom_rate += '0'
        else:
            epsilom_rate += '1'
    # convert to binary then decimal
    gamma_decimal = int(gamma_rate.encode('ASCII'), 2)
    epsilom_decimal = int(epsilom_rate.encode('ASCII'), 2)

    print(f'{day} answer = {gamma_decimal*epsilom_decimal}')


def day_three_two(day='Day three.two'):
    input_data_path = 'input_3.txt'
    oxygen_generator_rating = get_life_support_rating(input_data_path , most_common=True)
    co2_scrubber_rating = get_life_support_rating(input_data_path , most_common=False)

    print(f'{day} answer = {oxygen_generator_rating * co2_scrubber_rating}')


if __name__ == "__main__":
    day_one_one()
    day_one_two()
    day_two_one()
    day_two_two()
    day_three_one()
    day_three_two()




