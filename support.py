import pandas as pd
from statistics import mode


def create_data_frame(file_path):
    bit_data_frame = pd.DataFrame()
    with open(file_path) as file:
        for line in file:
            row = []
            # for each bit in the line:
            for bit in line.strip('\n'):
                # add bit to a list
                row.append(bit)
            # create a data frame with the list
            df = pd.Series(row)
            df_row = pd.DataFrame([df])
            # concat new dataframe to main dataframe
            bit_data_frame = pd.concat([bit_data_frame, df_row], ignore_index=True)
    return bit_data_frame


def get_most_common_bit(bit_list):
    most_common = mode(bit_list)
    # if the list is split evenly, 0s and 1s
    if bit_list.count(most_common) == len(bit_list)/2:
        most_common = '1'
    return most_common


def get_least_common_bit(bit_list):
    most_common = mode(bit_list)
    if bit_list.count(most_common) == len(bit_list)/2:
        return '0'
    else:
        if most_common == '0':
            return '1'
    return '0'


def update_data_frame(col_index, data_frame, most_common=True):
    if most_common:
        most = get_most_common_bit(data_frame[col_index].tolist())
        bit_data_frame = data_frame.loc[data_frame[col_index] == most]
    else:
        least = get_least_common_bit(data_frame[col_index].tolist())
        bit_data_frame = data_frame.loc[data_frame[col_index] == least]
    return bit_data_frame


def get_decimal_value_from_single_line_binary_data_frame(data_frame):
    binary_val = ''
    for index in range(len(data_frame.values[0])):
        binary_val += str(data_frame.values[0][index])
    # convert to decimal
    rating_value = int(binary_val.encode('ASCII'), 2)
    return rating_value


def get_life_support_rating(input_data, most_common):
    bit_data_frame = create_data_frame(input_data)
    for column in bit_data_frame.columns:
        # while there is still more than 1 row in the data set:
        if bit_data_frame.shape[0] > 1:
            # remove the rows from the data from that don't fit the criteria
            bit_data_frame = update_data_frame(column, bit_data_frame, most_common)
    return get_decimal_value_from_single_line_binary_data_frame(bit_data_frame)